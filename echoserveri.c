#include "csapp.h"
#include<string.h>

void echo(int connfd);

void eval(char *cmdline);

void sigchld_handler(int sig){
    while(waitpid(-1,0,WNOHANG)> 0)
        ;
    return;
}

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];
    
	Signal(SIGCHLD, sigchld_handler);
	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
        	if(Fork()==0){
            		Close(listenfd);
            		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
			haddrp = inet_ntoa(clientaddr.sin_addr);
			printf("server connected to %s (%s)\n", hp->h_name, haddrp);
            		echo(connfd);
            		Close(connfd);
            		exit(0);
        	}
		/* Determine the domain name and IP address of the client 
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);

		echo(connfd);*/
		Close(connfd);
	}
	exit(0);
}

void echo(int connfd)
{	
	int status;
	size_t n;
	char buf[MAXLINE];
	rio_t rio;
	pid_t pid;

	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		printf("Servidor recibio: %lu bytes\n", n);
		buf[strlen(buf) - 1] = 0;
		const char *argv[] = {buf, NULL};
		if (argv[0] == NULL)
			return;

		if((pid = Fork()) == 0){
			if (execve(argv[0], argv, environ) < 0){
				printf("%s: Comando no encontrado.\n", argv[0]);
				Rio_writen(connfd, "ERROR\n", strlen("ERROR\n"));
				exit(1);
			}
		}else{
			waitpid(pid, &status, 0);
			if (status == 0)
				Rio_writen(connfd, "OK\n", strlen("OK\n"));
		}
	}
}
